import Amplify from 'aws-amplify'
import config from './src/aws-exports'
config.Analytics = {disabled: true} // switch off Analytics to prevent warning messages
Amplify.configure(config)

import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import { withAuthenticator, AmplifyTheme } from 'aws-amplify-react-native' // this is the old variant

function HomeScreen({navigation}) {
  return (
    <View style={styles.container}>
      <Text style={styles.text}>This is the Home Page!</Text>
      <StatusBar style="auto" />

      <TouchableOpacity onPress={() => navigation.navigate('Second')} style={styles.button}>
        <Text style={styles.buttonText}>Next</Text>
      </TouchableOpacity>
    </View>
  );
}

function SecondScreen({navigation}) {
  return (
    <View style={styles.container}>
      <Text style={styles.text}>This is the Second Page!</Text>
      <StatusBar style="auto" />

      <TouchableOpacity onPress={() => navigation.goBack()} style={styles.button}>
        <Text style={styles.buttonText}>Back</Text>
      </TouchableOpacity>
    </View>
  );
}

const Stack = createStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator 
        initialRouteName="Home"
        screenOptions={{
          headerStyle: {
            backgroundColor: '#32ADE2',
            borderBottomWidth: 2,
          },
          headerTintColor: '#FEFFFF',
          headerTitleStyle: {
            fontWeight: 'bold',
        },
      }}>
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="Second" component={SecondScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const myAuthTheme = Object.assign({}, AmplifyTheme, {
  button: { backgroundColor: "#32ADE2", borderColor: "#FEFFFF" },
  buttonText: { color: "#FEFFFF" },
  sectionFooterLink: { color: "#32ADE2"},
  }
);

export default withAuthenticator(App, true, [], null, myAuthTheme);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#32ADE2',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    color: '#FEFFFF',
    width: '40%',
    fontSize: 20,
    textAlign: 'center',
  },
  button: {
    backgroundColor: '#32ADE2',
    padding: 20,
    borderRadius: 20,
    borderWidth: 2,
    borderColor: '#FEFFFF',
    marginTop: 50,
  },
  buttonText: {
    fontSize: 20,
    color: '#FEFFFF',
  }, 
});
